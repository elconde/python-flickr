"""Download all your photos from Flickr.

Caveat (from Flickr):

Please note that Flickr will return at most the first 4,000 results for
any given search query. If this is an issue, we recommend trying a more
specific query. """
import re
import xml.dom.minidom
import flickr
import argparse
import datetime
import time
import os
import urllib3

POOL_MANAGER = urllib3.PoolManager()
LOGGER = flickr.get_logger('download')
OUTPUT_DIR = os.path.join(os.path.expanduser('~'), 'Flickr-Download')


def parse_args():
    """Parse the command line arguments"""

    def to_date(arg_string):
        """String to date"""
        return datetime.datetime.strptime(arg_string, '%Y-%m-%d').date()

    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])
    parser.add_argument(
        '--start-date', help='Earliest photo upload date. Default=%(default)s',
        default='2018-03-20', type=to_date
    )
    return parser.parse_args()


def main():
    """Download all your photos from Flickr"""
    if not os.path.isdir(OUTPUT_DIR):
        LOGGER.info('mkdir %s', OUTPUT_DIR)
    args = parse_args()
    session = flickr.flickr_session()
    start_date: datetime.date = args.start_date
    start_date_in_seconds = datetime.datetime.combine(
        start_date, datetime.datetime.min.time()
    ).timestamp()
    # Download in daily chunks
    day = 60 * 60 * 24
    end_date_in_seconds = start_date_in_seconds + day - 1
    now = time.time()
    while end_date_in_seconds <= now:
        LOGGER.debug(
            '%s -> %s', time.ctime(start_date_in_seconds),
            time.ctime(end_date_in_seconds)
        )
        start_date_in_seconds = end_date_in_seconds + 1
        end_date_in_seconds = start_date_in_seconds + day -1
        response = get_response(
            session, start_date_in_seconds, end_date_in_seconds
        )
        number_of_pages = int(
            re.search(
                'pages="([^"]*)"', response.text.splitlines()[2]
            ).groups()[0]
        )
        for i in range(number_of_pages):
            page = i + 1
            response_page = get_response(
                session, start_date_in_seconds, end_date_in_seconds, page
            )
            document = xml.dom.minidom.parseString(response_page.text)
            for node in document.childNodes[0].childNodes[1].childNodes:
                if node.nodeType != 1:
                    continue
                attributes = {}
                for key in (
                    'farm', 'id', 'secret', 'server', 'title', 'url_o',
                ):
                    attributes[key] = node.getAttribute(key)
                dir_name = os.path.join(
                    OUTPUT_DIR, 'farm'+attributes['farm'], attributes['server']
                )
                if not os.path.isdir(dir_name):
                    os.makedirs(dir_name)
                url_o = attributes['url_o']
                id_ = attributes['id']
                if not url_o:
                    LOGGER.warning('Cannot download %s: No URL!', id_)
                    continue
                file_name = '{}.{}'.format(
                    attributes.get('title') or id_, url_o.split('.')[-1]
                )
                full_name = os.path.join(dir_name, file_name)
                if os.path.isfile(full_name):
                    continue
                LOGGER.info(full_name)
                with open(full_name, 'wb') as f_write:
                    f_write.write(POOL_MANAGER.request('GET', url_o).data)


def get_response(session, start_date_in_seconds, end_date_in_seconds, page=1):
    return session.get(
        flickr.REST_URL,
        params={
            'method': 'flickr.photos.search',
            'min_upload_date': start_date_in_seconds,
            'max_upload_date': end_date_in_seconds,
            'user_id': 'me', 'per_page': 500, 'page': page,
            'extras': 'url_o'
        }
    )


if __name__ == '__main__':
    main()
